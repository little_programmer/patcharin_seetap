﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace AlephApp
{
    public class Recent
    {
        public int id { get; set; }
        public string title { get; set; }
        public string category { get; set; }
        public string thumbnail { get; set; }
        public string url { get; set; }
        public string rating { get; set; }
        public Color color { get; set; }
    }

}
