# Aleph Xamarin Coding Test

Make sure you read **all** of this document carefully, and follow the guidelines in it. Pay particular attention to the "What We Care About" section.

## The task

Build an application with Xamarin Forms that fetches all of the recent projects from the [Aleph API](http://dev.aleph-labs.com/clients/api/projects.json) and displays them according to `mockup.png`.

Your data source is `http://dev.aleph-labs.com/clients/api/projects.json`. 

- Show each item's thumbnail image, name and rating. The name and image should link to the item's URL. 

- 5-star items should have a yellow background, as shown in the mockup.

- Make the design mockup work on small devices such as iPhone and Android.

- Tapping an item's `remove` link should remove it from the list (purely client side, this doesn't need to be communicated to the server).

## Getting started

We've supplied blank Visual Studio solution files under AlephApp folder.


Logo image assets that you may wish to use or edit are located in `assets`.

We encourage you to add comments explaining how your solution might differ if more tooling was available.

### Third party libraries

Remember, we want to see *your* code and *your* approach to the problem, rather than seeing how well you can use a particular library or framework. However, if you'd normally use a library for certain tasks (e.g. ajax or templating) feel free to include it.


### Tests

No need to worry about testing, we will test your work upon submission.

## What We Care About

We're interested in your method and how you approach the problem just as much as we're interested in the end result. We'll go through your code with you afterwards, and you can talk to us about how you tackled it, why you chose the approach you did, etc.

Here's what you should aim for:

- Clean, readable code that you'd expect to see in a real project. Would we want to work with your code as part of a bigger codebase?

- Good use of current front end performance best practices.

- Good cross-platform compatibility in the latest versions of popular mobile devices.

- Extensible code; when you come back in person we might ask you to add features.

- **Use git!** This is already a git repo. Commit small changes to it often so we can see your approach, and progress. Include the .git directory in the packaged .tar.gz file you send to us.

We haven't hidden any nasty tricks in the test. Don't overthink it. Just write nice, solid code.

## Submitting The Test

In your project directory, run:

```
tar -czvf firstname_lastname.tar.gz .
```
tar -czvf patcharin_seetap.tar.gz .
Then send the generated tar.gz file back to the person that sent you the test using WeTransfer.com
