﻿using Xamarin.Forms;
using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Net.Http;

namespace AlephApp
{
    public partial class AlephAppPage : ContentPage
    {
        List<Recent> Recents = new List<Recent>();

        public AlephAppPage()
        {
            InitializeComponent();
            this.GetDetail();
           
            lstRecent.ItemsSource = Recents;
        }

        void img_Tapped(object sender, EventArgs e)
        {
            TappedEventArgs Tap = (TappedEventArgs)e;
            for (int i = 0; i < Recents.Count; i++)
            {
                if (Recents[i].id == (int)Tap.Parameter)
                {
                    Device.OpenUri(new Uri(Recents[i].thumbnail)); ;
                }
            }
        }

        void lblTitle_Tapped(object sender, EventArgs e)
        {
            TappedEventArgs Tap = (TappedEventArgs)e;
            for (int i = 0; i < Recents.Count; i++)
            {
                if (Recents[i].id == (int)Tap.Parameter)
                {
                    Device.OpenUri(new Uri(Recents[i].url));;
                }
            }

        }

       async void lblRemove_Tapped(object sender, EventArgs e)
        {
            var anwer = await DisplayAlert("Delete?", "Do you want to delete this item?", "Yes", "No");
            if (anwer)
            {
                TappedEventArgs Tap = (TappedEventArgs)e;
                for(int i = 0; i < Recents.Count; i++)
                {
                    if(Recents[i].id==(int)Tap.Parameter)
                    {
                        Recents.Remove(Recents[i]);
                    }
                }
                lstRecent.ItemsSource = null;
                lstRecent.ItemsSource = Recents;
            }
        }

        private async void GetDetail()
        {
            var httpClient = new HttpClient();

            var response =  httpClient.GetAsync("http://dev.aleph-labs.com/clients/api/projects.json").Result;
            var body = await response.Content.ReadAsStringAsync();

            var json = JObject.Parse(body);

            var projects = json["projects"];

            var recent = projects["recent"];
            Recents = JsonConvert.DeserializeObject<List<Recent>>(recent.ToString());
            for (int i = 0; i < Recents.Count; i++)
            {
                if (Recents[i].rating == "5")
                {
                    Recents[i].color = Color.Yellow;

                }
                else
                {
                    Recents[i].color = Color.Silver;
                }
                Recents[i].id = i;
                Recents[i].rating = "Rating: " + Recents[i].rating;
            }
        }
    }
}
